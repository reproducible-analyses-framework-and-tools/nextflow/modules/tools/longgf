#!/usr/bin/env Nextflow

process longgf {
// require:
//   BAMS
//   params.longgf$gtf
//   params.longgf$longgf_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'longgf_container'
  label 'longgf'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/longgf", pattern: "*fusion*"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(bam)
  path gtf
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("${dataset}*fusion_predictions.tsv"), optional: true, emit: fusions

  script:
  """
  LongGF ${bam} ${gtf} ${parstr} > ${dataset}-${pat_name}-${run}.longgf_fusions
  """
}
